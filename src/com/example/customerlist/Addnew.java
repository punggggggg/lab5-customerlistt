package com.example.customerlist;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Addnew extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addnew);

		Button one = (Button) findViewById(R.id.button1);

		one.setOnClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.addnew, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {

		Intent data = new Intent();
		// TODO Auto-generated method stub
		EditText editText1 = (EditText) findViewById(R.id.editText1);
		data.putExtra("name", editText1.getText().toString());

		EditText editText2 = (EditText) findViewById(R.id.editText2);
		data.putExtra("phone", editText2.getText().toString());

		EditText editText3 = (EditText) findViewById(R.id.editText3);
		data.putExtra("package", editText3.getText().toString());

		if ((editText1.getText().toString().trim().isEmpty())
				|| (editText2.getText().toString().trim().isEmpty())
				|| (editText3.getText().toString().trim().isEmpty())) {

			AlertDialog dialog = new AlertDialog.Builder(this).create();
			dialog.setTitle("Error");
			dialog.setMessage("All fields are required!!!");
			dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			dialog.show();
		} else {
			this.setResult(RESULT_OK, data);
			this.finish();
		}
	}

}
